## Dockerrize botman studio development envionment

### How to use
```
git clone https://gitlab.com/rkdsay/laravel-botman-docker.git
cd laravel-botman-docker
docker-compose up -d --buld
docker-compose exec app composer install
```

The botman studio will be up running at `http://localhost:9000`


## Documentation of botman

You can find the BotMan and BotMan Studio documentation at [http://botman.io](http://botman.io).
